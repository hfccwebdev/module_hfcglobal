<?php

/**
 * @file
 * Documentation for HFC Custom Drupal Code.
 *
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen and the api.module in order to better organize
 * information about HFC custom projects.
 */

/**
 * @defgroup hfcc_modules HFC Custom Code
 * @{
 * This is a list of HFC custom code that is documented in this system.
 * @}
 */
