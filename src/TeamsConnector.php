<?php

namespace Drupal\hfcglobal;

use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hfcglobal\Event\HfcGlobalAlert;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;

/**
 * Defines the Catalog Archive Builder Service.
 *
 * @package Drupal\hfc_catalog_helper
 */
class TeamsConnector implements TeamsConnectorInterface {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * Stores the ConfigFactory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory|null
   */
  private $configFactory;

  /**
   * Stores the GuzzleHttp Client service.
   *
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * Stores the title of the message.
   *
   * @var string|null
   */
  private $title;

  /**
   * Stores the text of the message.
   *
   * @var string|null
   */
  private $text;

  /**
   * Stores the level type of the message.
   *
   * @var string|null
   */
  private $type;

  /**
   * Creates Constructor for these objects.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The Config Factory service.
   * @param \GuzzleHttp\Client $httpClient
   *   The Guzzle HTTP Client.
   */
  public function __construct(
    ConfigFactory $configFactory,
    Client $httpClient
  ) {
    $this->configFactory = $configFactory;
    $this->httpClient = $httpClient;
  }

  /**
   * Generate colour code for Teams Message output.
   *
   * @param string $type
   *   The message's type. Either self::TYPE_STATUS,
   *   self::TYPE_WARNING, or self::TYPE_ERROR.
   *
   * @return array
   *   return colour associated with type (warning level)
   */
  private function getStatusColor(string $type) {
    if ($type == 'warning') {
      return ['status' => '#FF8000', 'bg' => '#FF8000', 'text' => '#000'];
    }
    elseif ($type == 'error') {
      return ['status' => '#840000', 'bg' => '#840000', 'text' => '#fff'];
    }
    else {
      return ['status' => '#80EB6F', 'bg' => 'none', 'text' => 'none'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendToTeams($title, $text, $type = HfcGlobalAlert::TYPE_STATUS) {
    $teamsUrl = $this->configFactory->get("hfcglobal.notification_settings")->get("teams_webhook_url");
    if (empty($teamsUrl)) {
      $this->getLogger('hfcglobal')->error('TeamsConnect URL is not set in /admin/config/hfc/notification-settings form');
      return;
    }
    $themeColor = $this->getStatusColor($type);
    $outputTitle = Html::escape($title);
    $outputText = '<div style="padding: 10px; background-color: ' .
      $themeColor['bg'] . '; color: ' . $themeColor['text'] . '">' . Xss::filter($text) . '</div>';

    $response = $this->httpClient->request("POST", $teamsUrl, [
      RequestOptions::JSON => [
        "@type" => "MessageCard",
        "@context" => "https://schema.org/extensions",
        "title" => $outputTitle,
        "themeColor" => $themeColor['status'],
        "text" => $outputText,
      ],
    ]);
    if ($response->getStatusCode() != 200) {
      $this->getLogger('hfcglobal')->warning(
        'TeamsConnect message failure @code @reason.',
        [
          '@code' => $response->getStatusCode(),
          '@reason' => $response->getReasonPhrase(),
        ]
      );
    }
  }

}
