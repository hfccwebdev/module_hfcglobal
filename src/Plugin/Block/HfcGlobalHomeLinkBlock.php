<?php

namespace Drupal\hfcglobal\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'HfcGlobalHomeLinkBlock' block.
 *
 * @Block(
 *  id = "hfcglobal_home_link_block",
 *  admin_label = @Translation("HFC Global Home Link"),
 * )
 */
class HfcGlobalHomeLinkBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      [
        '#type' => 'link',
        '#title' => $this->t('HFC Home'),
        '#url' => Url::fromUri('https://www.hfcc.edu/'),
      ],
    ];
  }

}
