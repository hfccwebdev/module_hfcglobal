<?php

namespace Drupal\hfcglobal\Plugin\Block;

/**
 * Provides a 'Hfc Global Small Footer' block.
 *
 * @Block(
 *  id = "hfcglobal_small_footer_block",
 *  admin_label = @Translation("HFC Global Small Footer"),
 * )
 */
class HfcGlobalSmallFooterBlock extends HfcGlobalCompleteFooterBlock {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#prefix' => '<div class="complete-footer-wrapper content">',
      '#suffix' => '</div>',
    ];
    $build['copyright_info'] = [
      '#prefix' => '<div class="complete-footer-copyright">',
      'content' => $this->copyrightInfo(),
      '#suffix' => '</div>',
    ];
    $build['#attached']['library'][] = 'hfcglobal/hfcglobal-small-footer';
    return $build;
  }

}
