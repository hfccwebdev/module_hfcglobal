<?php

namespace Drupal\hfcglobal\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;

/**
 * Provides a 'LoginBlock' block.
 *
 * @Block(
 *  id = "login_block",
 *  admin_label = @Translation("Login links block"),
 * )
 */
class LoginBlock extends BlockBase {

  use RedirectDestinationTrait;

  /**
   * Link to HFC Password Help page.
   */
  const PASSWORD_HELP = 'https://www.hfcc.edu/password';

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['password_help_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Password Help link'),
      '#description' => $this->t('Provide the URL for the HFC Universal Password Help page.'),
      '#default_value' => isset($this->configuration['password_help_url'])
        ? $this->configuration['password_help_url']
        : self::PASSWORD_HELP,
      '#required' => TRUE,
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['password_help_url'] = $form_state->getValue('password_help_url');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $options = ['attributes' => ['class' => ['hfc-button']]];
    $password_help = isset($this->configuration['password_help_url']) ? $this->configuration['password_help_url'] : 'https://my.hfcc.edu/passwords';
    $url = Url::fromUri($password_help);
    $url->setOptions($options);
    $build['login_block_password_help'] = [
      '#prefix' => '<p>',
      '#markup' => Link::fromTextAndUrl('Password Help', $url, $options)->toString(),
      '#suffix' => '</p>',
      '#weight' => 10,
    ];

    $build['login_block_link'] = [
      '#prefix' => '<p>',
      '#markup' => Link::createFromRoute('Log In', 'user.login', $this->getDestinationArray(), $options)->toString(),
      '#suffix' => '</p>',
      '#weight' => -10,
    ];

    return $build;
  }

}
