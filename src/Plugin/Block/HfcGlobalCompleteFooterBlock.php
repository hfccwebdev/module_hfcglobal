<?php

namespace Drupal\hfcglobal\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'HfcGlobalCompleteFooterBlock' block.
 *
 * @Block(
 *  id = "hfcglobal_complete_footer_block",
 *  admin_label = @Translation("HFC Global Complete Footer"),
 * )
 */
class HfcGlobalCompleteFooterBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Default implementation for the file URL generator service.
   *
   * @var \Drupal\Core\File\FileUrlGenerator
   */
  private $fileUrlGenerator;

  /**
   * Gets the processed active profile object, or null.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  private $moduleExtensionList;

  /**
   * Stores the path to this module.
   *
   * @var string
   */
  private $modulePath;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_url_generator'),
      $container->get('extension.list.module')
    );
  }

  /**
   * Constructs a new footer block object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\File\FileUrlGenerator $fileUrlGenerator
   *   The implementation for the file URL generator service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The processed active profile object, or null.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FileUrlGenerator $fileUrlGenerator,
    ModuleExtensionList $moduleExtensionList
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->moduleExtensionList = $moduleExtensionList;
    $this->modulePath = $this->moduleExtensionList->getPath('hfcglobal');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [
      '#prefix' => '<div id="complete-footer-wrapper" class="complete-footer-wrapper content">',
      '#suffix' => '</div>',
    ];

    $build['logo_block'] = [
      '#prefix' => '<div id="complete-footer-logo" class="complete-footer-logo no-link">',
      'content' => $this->logoBlock(),
      '#suffix' => '</div>',
    ];
    $build['contact_info'] = [
      '#prefix' => '<div id="complete-footer-contact" class="complete-footer-contact">',
      'content' => $this->contactInfo(),
      '#suffix' => '</div>',
    ];
    $build['footer_links'] = [
      '#prefix' => '<div id="complete-footer-links" class="complete-footer-links">',
      'content' => $this->footerLinks(),
      '#suffix' => '</div>',
    ];
    $build['transparency_reporting'] = [
      '#prefix' => '<div id="complete-footer-transparency" class="complete-footer-transparency no-link">',
      'content' => $this->transparencyReporting(),
      '#suffix' => '</div>',
    ];
    $build['social_icons'] = [
      '#prefix' => '<div id="complete-footer-social" class="complete-footer-social">',
      'content' => $this->socialIcons(),
      '#suffix' => '</div>',
    ];
    $build['copyright_info'] = [
      '#prefix' => '<div id="complete-footer-copyright" class="complete-footer-copyright">',
      'content' => $this->copyrightInfo(),
      '#suffix' => '</div>',
    ];
    $build['#attached']['library'][] = 'hfcglobal/hfcglobal-complete-footer';
    return $build;
  }

  /**
   * Generate the logo output.
   */
  private function logoBlock() {

    $image = [
      '#theme' => 'image',
      '#uri' => $this->fileUrlGenerator->generateAbsoluteString("{$this->modulePath}/images/hfc-futuredriven-white.svg"),
      '#alt' => $this->t('Henry Ford College – Future Driven'),
    ];

    return [
      '#type' => 'link',
      '#title' => $image,
      '#url' => Url::fromUri('https://www.hfcc.edu/'),
      '#options' => [
        'attributes' => [
          'class' => ['futuredriven-logo'],
          'title' => $this->t('Henry Ford College – Future Driven'),
        ],
      ],
    ];
  }

  /**
   * Generate footer link list.
   */
  private function footerLinks() {

    $links = [
      [
        '#type' => 'link',
        '#title' => $this->t('HFC Careers'),
        '#url' => Url::fromUri('https://www.hfcc.edu/human-resources'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Media Information'),
        '#url' => Url::fromUri('https://marcom.hfcc.edu/media'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Campus Safety Information and Resources'),
        '#url' => Url::fromUri('https://www.hfcc.edu/campus-safety'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Request Information'),
        '#url' => Url::fromUri('https://www.hfcc.edu/more-info'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Accreditation'),
        '#url' => Url::fromUri('https://www.hfcc.edu/accreditation'),
      ],
    ];

    return [
      '#theme' => 'item_list',
      '#items' => $links,
      '#attributes' => ['class' => 'menu'],
    ];
  }

  /**
   * Generate the transparency reporting link.
   */
  private function transparencyReporting() {

    $image = [
      '#theme' => 'image',
      '#uri' => $this->fileUrlGenerator->generateAbsoluteString("{$this->modulePath}/images/budget-transparency-reporting.png"),
      '#alt' => $this->t('Budget and Performance Transparency Reporting'),
    ];

    return [
      '#type' => 'link',
      '#title' => $image,
      '#url' => Url::fromUri('https://www.hfcc.edu/'),
      '#options' => [
        'attributes' => [
          'class' => ['transp-reporting-logo'],
          'title' => $this->t('Budget and Performance Transparency Reporting'),
        ],
      ],
    ];
  }

  /**
   * Generate the copyright info.
   */
  protected function copyrightInfo() {

    $year = date('Y');
    $links = [
      [
        '#markup' => $this->t('<a href="https://www.hfcc.edu/copyright">Copyright &copy;@year</a> Henry Ford College All rights reserved.', ['@year' => $year]),
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Privacy Policy'),
        '#url' => Url::fromUri('https://www.hfcc.edu/about-us/privacy'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Terms of Use'),
        '#url' => Url::fromUri('https://my.hfcc.edu/aup'),
      ],
    ];

    return [
      '#theme' => 'item_list',
      '#items' => $links,
      '#attributes' => ['class' => 'inline'],
    ];
  }

  /**
   * Generate the contact info.
   */
  public function contactInfo() {

    $links = [
      [
        '#markup' => $this->t('Henry Ford College <br>5101 Evergreen Rd.<br>Dearborn, MI 48128'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Campus Map'),
        '#url' => Url::fromUri('https://www.hfcc.edu/map'),
        '#options' => [
          'attributes' => [
            'class' => ['contact-border'],
          ],
        ],
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('313-845-9600'),
        '#url' => Url::fromUri('tel:1-313-845-9600'),
        '#options' => [
          'attributes' => [
            'class' => ['no-link'],
          ],
        ],
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Contact Us'),
        '#url' => Url::fromUri('https://www.hfcc.edu/directory'),
      ],
    ];

    return [
      '#theme' => 'item_list',
      '#items' => $links,
      '#attributes' => ['class' => 'menu'],
    ];
  }

  /**
   * Generate the social icons.
   */
  private function socialIcons() {
    $links = [
      [
        '#type' => 'link',
        '#title' => $this->t('Facebook'),
        '#url' => Url::fromUri('https://www.facebook.com/henryfordcc'),
        '#options' => [
          'attributes' => [
            'class' => ['no-link social-fb'],
          ],
        ],
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Twitter'),
        '#url' => Url::fromUri('https://twitter.com/hfcc'),
        '#options' => [
          'attributes' => [
            'class' => ['no-link social-twitter'],
          ],
        ],
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('LinkedIn'),
        '#url' => Url::fromUri('https://www.linkedin.com/in/henry-ford-college-19737833/'),
        '#options' => [
          'attributes' => [
            'class' => ['no-link social-linkedin'],
          ],
        ],
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('YouTube'),
        '#url' => Url::fromUri('https://www.youtube.com/channel/UCmpw07OdQf5XTCTA27aOWmA'),
        '#options' => [
          'attributes' => [
            'class' => ['no-link social-youtube'],
          ],
        ],
      ],
    ];

    return [
      '#theme' => 'item_list',
      '#items' => $links,
      '#attributes' => ['class' => 'menu'],
    ];
  }

}
