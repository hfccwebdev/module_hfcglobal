<?php

namespace Drupal\hfcglobal\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'HfcGlobalUniversalNavBlock' block.
 *
 * @Block(
 *  id = "hfcglobal_universal_nav_block",
 *  admin_label = @Translation("HFC Global Universal Navigation"),
 * )
 */
class HfcGlobalUniversalNavBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      [
        '#type' => 'link',
        '#title' => $this->t('Students'),
        '#url' => Url::fromUri('https://my.hfcc.edu/students'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Employees'),
        '#url' => Url::fromUri('https://my.hfcc.edu/faculty-and-staff'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Alumni'),
        '#url' => Url::fromUri('https://www.hfcc.edu/alumni'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Community'),
        '#url' => Url::fromUri('https://www.hfcc.edu/about/community'),
      ],
      [
        '#type' => 'link',
        '#title' => $this->t('Give to HFC'),
        '#url' => Url::fromUri('https://foundation.hfcc.edu/'),
      ],
    ];
  }

}
