<?php

namespace Drupal\hfcglobal\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event to provide a general notification.
 */
class HfcGlobalNotification extends Event {

  /**
   * The event name.
   */
  const EVENT_NAME = 'notification';

  /**
   * The message title.
   *
   * @var string
   */
  public $title;

  /**
   * The message destination.
   *
   * @var string
   */
  public $destination;

  /**
   * The message body.
   *
   * @var string
   */
  public $body;

  /**
   * Message options.
   *
   * @var array
   */
  public $options;

  /**
   * Creates a new instance of this class.
   *
   * @param string $title
   *   The message title.
   * @param string $destination
   *   The message destination. Usually an email address.
   * @param string $body
   *   The message body.
   * @param array $options
   *   Message options.
   */
  public static function create($title, $destination, $body, array $options = []) {
    return new static($title, $destination, $body, $options);
  }

  /**
   * Constructs the object.
   *
   * @param string $title
   *   The message title.
   * @param string $destination
   *   The message destination.
   * @param string $body
   *   The message body.
   * @param array $options
   *   Message options.
   */
  public function __construct($title, $destination, $body, array $options = []) {
    $this->title = $title;
    $this->destination = $destination;
    $this->body = $body;
    $this->options = $options;
  }

  /**
   * Returns the message values.
   *
   * @return string[]
   *   The stored message values.
   */
  public function getMessage(): array {
    return [
      'title' => $this->title,
      'destination' => $this->destination,
      'body' => $this->body,
      'options' => $this->options,
    ];
  }

}
