<?php

namespace Drupal\hfcglobal\Event;

/**
 * Defines events for the hfcglobal module.
 */
final class HfcGlobalEvents {

  /**
   * HFC Global Alert event definition.
   *
   * The event subscriber method receives a
   * \Drupal\hfcglobal\Event\HfcGlobalAlert instance.
   *
   * @Event
   *
   * @var string
   */
  const HFC_GLOBAL_ALERT = 'hfcglobal.alert';

  /**
   * HFC Global Notification event definition.
   *
   * The event subscriber method receives a
   * \Drupal\hfcglobal\Event\HfcGlobalNotification instance.
   *
   * @Event
   *
   * @var string
   */
  const HFC_GLOBAL_NOTIFICATION = 'hfcglobal.notification';

}
