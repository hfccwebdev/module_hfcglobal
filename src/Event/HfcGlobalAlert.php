<?php

namespace Drupal\hfcglobal\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event to provide a global alerting mechanism.
 */
class HfcGlobalAlert extends Event {

  /**
   * The event name.
   */
  const EVENT_NAME = 'alert';

  /**
   * Alert type status message.
   */
  const TYPE_STATUS = 'status';

  /**
   * Alert type warning.
   */
  const TYPE_WARNING = 'warning';

  /**
   * Alert type error.
   */
  const TYPE_ERROR = 'error';

  /**
   * The message title.
   *
   * @var string
   */
  public $title;

  /**
   * The message text.
   *
   * @var string
   */
  public $text;

  /**
   * The message type.
   *
   * @var string
   */
  public $type;

  /**
   * Creates a new instance of this class.
   *
   * @param string $title
   *   The alert message title.
   * @param string $text
   *   The message to be displayed to the user. For consistency
   *   with other messages, it should begin with a capital letter
   *   and end with a period.
   * @param string $type
   *   (optional) The message's type. Either self::TYPE_STATUS,
   *   self::TYPE_WARNING, or self::TYPE_ERROR.
   */
  public static function create($title, $text, $type = self::TYPE_STATUS) {
    return new static($title, $text, $type);
  }

  /**
   * Constructs the object.
   *
   * @param string $title
   *   The alert message title.
   * @param string $text
   *   The message to be displayed to the user. For consistency
   *   with other messages, it should begin with a capital letter
   *   and end with a period.
   * @param string $type
   *   (optional) The message's type. Either self::TYPE_STATUS,
   *   self::TYPE_WARNING, or self::TYPE_ERROR.
   */
  public function __construct($title, $text, $type = self::TYPE_STATUS) {
    $this->title = $title;
    $this->text = $text;
    $this->type = $type;
  }

  /**
   * Returns the message values.
   *
   * @return string[]
   *   The stored message values.
   */
  public function getMessage() {
    return [
      'title' => $this->title,
      'text' => $this->text,
      'type' => $this->type,
    ];
  }

}
