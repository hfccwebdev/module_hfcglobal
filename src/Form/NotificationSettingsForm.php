<?php

namespace Drupal\hfcglobal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the Notification Settings Form.
 */
class NotificationSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'hfcglobal.notification_settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfcglobal_notification_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['teams_webhook_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Microsoft Teams webhook URL'),
      '#default_value' => $config->get('teams_webhook_url'),
      '#maxlength' => 248,
    ];

    $form['email_disabled'] = [
      '#type' => 'radios',
      '#title' => $this->t('Disable email notifications'),
      '#options' => [
        TRUE => 'Yes',
        FALSE => 'No',
      ],
      '#default_value' => $config->get('email_disabled'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve and store the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('teams_webhook_url', $form_state->getValue('teams_webhook_url'))
      ->save();
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('email_disabled', $form_state->getValue('email_disabled'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
