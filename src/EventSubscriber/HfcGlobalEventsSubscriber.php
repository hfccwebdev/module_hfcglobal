<?php

namespace Drupal\hfcglobal\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\hfcglobal\Event\HfcGlobalAlert;
use Drupal\hfcglobal\Event\HfcGlobalEvents;
use Drupal\hfcglobal\Event\HfcGlobalNotification;
use Drupal\hfcglobal\TeamsConnectorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Defines the HFC Global Notification Events subscriber.
 *
 * @package Drupal\hfcglobal\EventSubscriber
 */
class HfcGlobalEventsSubscriber implements EventSubscriberInterface {

  use LoggerChannelTrait;

  /**
   * Stores the Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Stores the Plugin Mail Manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  private $mailManager;

  /**
   * Stores the Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * Stores Teams Connector service.
   *
   * @var \Drupal\hfcglobal\TeamsConnectorInterface
   */
  private $teamsConnector;

  /**
   * Class Constructor for HFC Global Events Subscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The Plugin Mail Manager service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The Renderer service.
   * @param \Drupal\hfcglobal\TeamsConnectorInterface $teamsConnector
   *   The Teams Connector Service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    MailManagerInterface $mailManager,
    RendererInterface $renderer,
    TeamsConnectorInterface $teamsConnector
  ) {
    $this->configFactory = $config_factory;
    $this->mailManager = $mailManager;
    $this->renderer = $renderer;
    $this->teamsConnector = $teamsConnector;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      HfcGlobalEvents::HFC_GLOBAL_ALERT => 'sendTeamsAlert',
      HfcGlobalEvents::HFC_GLOBAL_NOTIFICATION => 'sendEmailNotification',
    ];
  }

  /**
   * Send HFC Global Alert message to Microsoft Teams.
   *
   * @param \Drupal\hfcglobal\Event\HfcGlobalAlert $event
   *   HFC Global Alert event.
   */
  public function sendTeamsAlert(HfcGlobalAlert $event) {
    $message = $event->getMessage();
    $this->teamsConnector->sendToTeams($message['title'], $message['text'], $message['type']);
  }

  /**
   * Send HFC Global Notification message to email recipients.
   *
   * @param \Drupal\hfcglobal\Event\HfcGlobalNotification $event
   *   HFC Global Notification event.
   */
  public function sendEmailNotification(HfcGlobalNotification $event) {

    // Load the notification data.
    $notification = $event->getMessage();

    // Discard messages if notifications are disabled.
    if ($this->configFactory->get('hfcglobal.notification_settings')->get('email_disabled')) {
      $this->getLogger('hfcglobal')->info(
        'Discard email notification @subject.',
        ['@subject' => $notification['title']]
      );
      return;
    }

    // @todo Make sure this is a valid email address.
    if (!isset($notification['destination'])) {
      $this->getLogger('hfcglobal')->error('HFC_GLOBAL_NOTIFICATION received with no destination address.');
      return;
    }

    // Build the mail parameters.
    $params = [];
    if (isset($notification['options']['sender'])) {
      $params['sender'] = $notification['options']['sender'];
    }
    if (!empty($notification['title'])) {
      $params['subject'] = $notification['title'];
    }
    if (!empty($notification['body'])) {
      $params['body'] = is_array($notification['body'])
        ? $this->renderer->renderRoot($notification['body'])
        : $notification['body'];
    }

    $result = $this->mailManager->mail(
      "hfcglobal",
      "notification",
      $notification['destination'],
      LanguageInterface::LANGCODE_NOT_SPECIFIED,
      $params
    );

    if ($result['result'] === FALSE) {
      $this->getLogger('hfcglobal')->error(
        'HFC_GLOBAL_NOTIFICATION email failed: @subject.',
        ['@subject' => $result['subject']]
      );
    }
  }

}
