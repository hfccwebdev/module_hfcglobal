<?php

namespace Drupal\hfcglobal;

use Drupal\hfcglobal\Event\HfcGlobalAlert;

/**
 * Defines the Teams Connector Interface.
 *
 * @package Drupal\hfcglobal
 */
interface TeamsConnectorInterface {

  /**
   * Generate Teams Message output.
   *
   * @param string $title
   *   The alert message title.
   * @param string $text
   *   The message to be displayed to the user. For consistency
   *   with other messages, it should begin with a capital letter
   *   and end with a period.
   * @param string $type
   *   (optional) The message's type. Either self::TYPE_STATUS,
   *   self::TYPE_WARNING, or self::TYPE_ERROR.
   */
  public function sendToTeams($title, $text, $type = HfcGlobalAlert::TYPE_STATUS);

}
