<?php

namespace Drupal\hfcglobal\Controller;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\Response;

/**
 * Defines the NewRelic Heartbeat Controller.
 *
 * We no longer use NewRelic as a service, but this page callback
 * can still generate heartbeat content for other external monitoring.
 */
class NewRelicHeartbeatController {

  use StringTranslationTrait;

  /**
   * Generates content for the heartbeat.
   */
  public function content() {
    // Determine when cron last ran.
    // @see system_requirements()
    $cron_last = \Drupal::state()->get('system.cron_last');
    if (!is_numeric($cron_last)) {
      $cron_last = \Drupal::state()->get('install_time', 0);
    }
    return new Response($this->t(
      "cron_last: @i ago\n", ['@i' => time() - $cron_last]
    ));
  }

}
