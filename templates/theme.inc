<?php

/**
 * @file
 * Theme functions for HFC GLOBAL module.
 */

use Drupal\Core\Template\Attribute;

/**
 * Processes variables for hfcglobal-pseudo-field.html.twig template.
 */
function hfcglobal_preprocess_hfcglobal_pseudo_field(&$variables) {

  $element = $variables['element'];

  if (!empty($element['#markup']) || !empty($element['#items'])) {
    $variables['field_name'] = $element['#field_name'] ?? 'none';
    $variables['label_display'] = $element['#label_display'] ?? 'above';
    if (!empty($element['#label'])) {
      $variables['label'] = $element['#label'];
    }
  }

  if (!empty($element['#markup'])) {
    $variables['items'][] = [
      'content' => $element['#markup'],
      'attributes' => new Attribute(),
    ];
  }
  elseif (!empty($element['#items']) && is_array($element['#items'])) {
    foreach ($element['#items'] as $delta => $item) {
      if (!isset($item['attributes'])) {
        $item['attributes'] = new Attribute();
      }
      if (isset($item['#classes'])) {
        foreach ($item['#classes'] as $class) {
          $item['attributes']->addClass($class);
        }
      }
      $variables['items'][$delta] = $item;
    }
  }
}
